import argparse
import logging
import os
import time
import warnings

from cobra.io import read_sbml_model
from management.metabolic_model import model_editing
from management.metabolic_model import model_statistics


def main():
    warnings.filterwarnings("ignore")
    start = time.time()
    
    parser = argparse.ArgumentParser()

    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")
    parser.add_argument('-medium', '--medium_file', required=True, help="Medium file")

    
    options = parser.parse_args()
    output_dir = options.output_dir
    model_file = options.cobra_model_file
    medium_file = options.medium_file
    
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
        
    try:
        os.mkdir(output_dir)
    except:
        pass

    ## read cobra model
    cobra_model = read_sbml_model(model_file)

    simulation_cobra_model = model_editing.make_medium_model(cobra_model, medium_file)

    # model statistics
    model_stat_info = model_statistics.calculate_model_statistics(simulation_cobra_model)
    with open(output_dir + 'model_statistics.txt', 'w') as fp:
        for each_key in model_stat_info:
            print >> fp, '%s\t%s' % (each_key, model_stat_info[each_key])

    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))


if __name__ == '__main__':
    main()
