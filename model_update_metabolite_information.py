import argparse
import logging
import os
import time
import warnings

from cobra.io import read_sbml_model
from cobra.io.sbml import write_cobra_model_to_sbml_file
from management.metabolic_model import model_editing


def main():    
    warnings.filterwarnings("ignore")
    start = time.time()
    
    parser = argparse.ArgumentParser()

    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")
    parser.add_argument('-mnx_xref', '--MNXref_metabolite_xref', required=True, help="MNXref xref file")
    parser.add_argument('-mnx_prop', '--MNXref_metabolite_prop', required=True, help="MNXref prop file")
    parser.add_argument('-bigg', '--BIGG_metabolite_information', required=True, help="BIGG metabolite information file")
    parser.add_argument('-chebi', '--CHEBI_metabolite_information', required=True, help="CHEBI metabolite information file")
    
    options = parser.parse_args()
    output_dir = options.output_dir
    model_file = options.cobra_model_file
    MNXref_metabolite_xref = options.MNXref_metabolite_xref
    MNXref_metabolite_prop = options.MNXref_metabolite_prop
    BIGG_metabolite_information = options.BIGG_metabolite_information
    CHEBI_metabolite_information = options.CHEBI_metabolite_information

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
        
    try:
        os.mkdir(output_dir)
    except:
        pass

    model_basename = os.path.basename(model_file)
    output_file = output_dir + 'Metabolite_%s' % (model_basename)

    cobra_model = read_sbml_model(model_file)

    cobra_model, metabolite_information = model_editing.update_metabolite_information(cobra_model,
                                                                                      BIGG_metabolite_information,
                                                                                      CHEBI_metabolite_information,
                                                                                      MNXref_metabolite_prop,
                                                                                      MNXref_metabolite_xref)

    write_cobra_model_to_sbml_file(cobra_model, output_file, use_fbc_package=False)

    with open(output_dir + 'Metabolite_information.txt', 'w') as fp:
        print >> fp, 'Metabolite ID\tMetabolite name\tFormula\tCharge\tInChI\tSmiles\tReferences'
        for each_metabolite in metabolite_information:
            metabolite_name = metabolite_information[each_metabolite]['name']
            formula = metabolite_information[each_metabolite]['formula']
            charge = metabolite_information[each_metabolite]['charge']
            inchi = metabolite_information[each_metabolite]['inchi']
            smiles = metabolite_information[each_metabolite]['smiles']
            references = metabolite_information[each_metabolite]['Reference']
            print >> fp, '%s\t%s\t%s\t%s\t%s\t%s\t%s' % (
                each_metabolite, metabolite_name, formula, charge, inchi, smiles, references)
            
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))

if __name__ == '__main__':
    main()
