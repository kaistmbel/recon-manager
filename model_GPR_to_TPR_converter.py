import argparse
import logging
import os
import time
import warnings

from cobra.io import read_sbml_model
from management.metabolic_model import GPR_Converter


def main():    
    warnings.filterwarnings("ignore")
    parser = argparse.ArgumentParser()

    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")
    parser.add_argument('-gene_transcript_information', '--gene_transcript_information_file', required=True,
                        help="Gene transcript information file")
    
    
    options = parser.parse_args()
    output_dir = options.output_dir
    model_file = options.cobra_model_file
    gene_transcript_information_file = options.gene_transcript_information_file

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
        
    try:
        os.mkdir(output_dir)
    except:
        pass

    cobra_model = read_sbml_model(model_file)
    model_name = os.path.basename(model_file).split('.xml')[0].strip()
    gpr_obj = GPR_Converter.GPR_Converter()
    gpr_obj.load_metabolic_model(cobra_model)
    gpr_obj.load_gene_transcript_mapping_file(gene_transcript_information_file)
    
    logging.info("Converting GPR associations to TPR associations")
    start = time.time()
    gpr_obj.convert_GPR_to_TPR(output_dir + '%s_Ensembl_Transcript.xml' % (model_name), 'ensembl_transcript')
    gpr_obj.convert_GPR_to_TPR(output_dir + '%s_UCSC_Transcript.xml' % (model_name), 'ucsc')
    gpr_obj.convert_GPR_to_TPR(output_dir + '%s_RefSeq_Transcript.xml' % (model_name), 'ref_mrna')    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
if __name__ == '__main__':
    main()
