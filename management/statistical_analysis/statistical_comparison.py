import numpy as np
import pandas as pd
from scipy.stats import mannwhitneyu
from scipy.stats import ranksums
from scipy.stats import shapiro
from scipy.stats import ttest_ind
from statsmodels.stats.multitest import fdrcorrection


def two_grouped_data_comparison(condition1_df, condition2_df):
    comparison_results = {}
    index_list = []
    p_value_list = []
    method_list = []
    data_information = {}
    for each_index in set(condition1_df.index) & set(condition2_df.index):
        method = 'N/A'
        pop1 = list(condition1_df.ix[each_index].values)
        pop2 = list(condition2_df.ix[each_index].values)

        pop1_size = len(pop1)
        pop2_size = len(pop2)

        if pop1_size < 10 or pop2_size < 10:
            statistics, pvalue = ranksums(pop1, pop2)
            method = 'Wilcoxon rank sum'
        else:
            statistics1, p_value1 = shapiro(pop1)
            statistics2, p_value2 = shapiro(pop2)
            if p_value1 > 0.05 and p_value2 > 0.05:
                statistics, pvalue = ttest_ind(pop1, pop2)
                method = 'Independent t test'
            else:
                statistics, pvalue = mannwhitneyu(pop1, pop2)
                method = 'Mann Whitney U test'

        p_value_list.append(pvalue)
        method_list.append(method)
        index_list.append(each_index)
        data_information[each_index] = {}
        data_information[each_index]['Log2 FC (pop1/pop2)'] = np.log2(np.mean(pop1) / np.mean(pop2))
        data_information[each_index]['Condition1 mean'] = np.mean(pop1)
        data_information[each_index]['Condition2 mean'] = np.mean(pop2)
        data_information[each_index]['Condition1 std'] = np.std(pop1)
        data_information[each_index]['Condition2 std'] = np.std(pop2)

    rejected, p_value_corrected = fdrcorrection(p_value_list)
    for i in range(len(p_value_list)):
        index_id = index_list[i]
        comparison_results[index_id] = {}
        comparison_results[index_id]['Method'] = method_list[i]
        comparison_results[index_id]['FDR corrected P'] = p_value_corrected[i]
        comparison_results[index_id]['P'] = p_value_list[i]
        comparison_results[index_id]['Condition1 mean'] = data_information[index_id]['Condition1 mean']
        comparison_results[index_id]['Condition2 mean'] = data_information[index_id]['Condition2 mean']
        comparison_results[index_id]['Condition1 std'] = data_information[index_id]['Condition1 std']
        comparison_results[index_id]['Condition2 std'] = data_information[index_id]['Condition2 std']
        comparison_results[index_id]['Log2 FC (pop1/pop2)'] = data_information[index_id]['Log2 Fold (pop1/pop2)']

    comparison_result_df = pd.DataFrame.from_dict(comparison_results)
    return comparison_result_df.T
