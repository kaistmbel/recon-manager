import numpy as np
import pandas as pd
from scipy import stats
from statsmodels.stats.multitest import fdrcorrection


def pathway_enrichment_analysis(cobra_model, selected_reactions):
    pathway_enrichment_result_info = {}
    significantly_over_represented_pathways = []
    all_reactions = [each_reaction.id for each_reaction in cobra_model.reactions]

    pathway_info = {}
    for each_reaction in cobra_model.reactions:
        subsystem = each_reaction.subsystem
        if subsystem not in pathway_info:
            pathway_info[subsystem] = [each_reaction.id]
        else:
            pathway_info[subsystem].append(each_reaction.id)

    p_value_list = []
    pathway_list = []
    odd_ratio_list = []
    data_information = {}
    for each_pathway in pathway_info:
        pathway_reactions = pathway_info[each_pathway]
        no_pathway_reactions = set(all_reactions).difference(pathway_reactions)
        not_selected_reactions = set(all_reactions).difference(set(selected_reactions))

        not_selected_reactions_no_pathway = set(no_pathway_reactions) & set(not_selected_reactions)
        not_selected_reactions_pathway = set(pathway_reactions) & set(not_selected_reactions)

        selected_reactions_no_pathway = set(no_pathway_reactions) & set(selected_reactions)
        selected_reactions_pathway = set(pathway_reactions) & set(selected_reactions)

        oddsratio, p_value = stats.fisher_exact(
            [[len(not_selected_reactions_no_pathway), len(not_selected_reactions_pathway)],
             [len(selected_reactions_no_pathway), len(selected_reactions_pathway)]])
        p_value_list.append(p_value)
        odd_ratio_list.append(oddsratio)
        pathway_list.append(each_pathway)
        data_information[each_pathway] = {}
        data_information[each_pathway]['No. of selected reactions in pathway'] = len(selected_reactions_pathway)
        data_information[each_pathway]['No. of all reactions in pathway'] = len(pathway_reactions)
    rejected, p_value_corrected = fdrcorrection(p_value_list)
    for i in range(len(p_value_corrected)):
        pathway = pathway_list[i]
        pathway_enrichment_result_info[pathway] = {}
        pathway_enrichment_result_info[pathway]['FDR corrected P'] = p_value_corrected[i]
        pathway_enrichment_result_info[pathway]['P'] = p_value_list[i]
        pathway_enrichment_result_info[pathway]['No. of selected reactions in pathway'] = data_information[pathway][
            'No. of selected reactions in pathway']
        pathway_enrichment_result_info[pathway]['No. of all reactions in pathway'] = data_information[pathway][
            'No. of all reactions in pathway']
    pathway_enrichment_result_df = pd.DataFrame.from_dict(pathway_enrichment_result_info)
    return pathway_enrichment_result_df.T


def model_contents_enrichment_analysis(model_content1_df, model_content2_df):
    analysis_type_list = ['Reaction', 'Metabolite', 'Gene']
    model_content_enrichment_result_info = {}
    for each_analysis_type in analysis_type_list:
        model_content_enrichment_result_info[each_analysis_type] = {}
        content1_df = model_content1_df[model_content1_df['Variable type'] == each_analysis_type]
        content2_df = model_content2_df[model_content2_df['Variable type'] == each_analysis_type]

        condition1_total_cnt = len(set(model_content1_df['Name']))
        condition2_total_cnt = len(set(model_content2_df['Name']))
        common_contents = list(set(content1_df['ID']) & set(content2_df['ID']))
        p_value_list = []
        content_list = []
        content_data_list = []
        log2_fold_list = []
        for each_id in common_contents:
            condition1_list = list(content1_df['ID'] == each_id)
            condition2_list = list(content2_df['ID'] == each_id)

            condition1_true_cnt = condition1_list.count(True)
            condition2_true_cnt = condition2_list.count(True)

            oddsratio, p_value = stats.fisher_exact([[condition1_true_cnt, condition2_true_cnt],
                                                     [condition1_total_cnt - condition1_true_cnt,
                                                      condition2_total_cnt - condition2_true_cnt]])

            p_value_list.append(p_value)
            content_list.append(each_id)
            content_data_list.append([condition1_true_cnt, condition2_true_cnt])
            log2_fold_list.append(np.log2(condition1_true_cnt / float(condition2_true_cnt)))

        rejected, p_value_corrected = fdrcorrection(p_value_list)
        enrichment_result_info = {}
        for i in range(len(p_value_corrected)):
            each_id = content_list[i]
            enrichment_result_info[each_id] = {}
            enrichment_result_info[each_id]['FDR corrected P'] = p_value_corrected[i]
            enrichment_result_info[each_id]['P'] = p_value_list[i]
            enrichment_result_info[each_id]['Log2 FC (condition1/condition2)'] = log2_fold_list[i]
            enrichment_result_info[each_id]['No. of occurrences in condition1'] = content_data_list[i][0]
            enrichment_result_info[each_id]['No. of occurrences in condition2'] = content_data_list[i][1]
        enrichmenet_df = pd.DataFrame.from_dict(enrichment_result_info)
        model_content_enrichment_result_info[each_analysis_type] = enrichmenet_df.T

    return model_content_enrichment_result_info['Reaction'], model_content_enrichment_result_info['Metabolite'], \
           model_content_enrichment_result_info['Gene']
