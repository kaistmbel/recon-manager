import os
import tempfile

from cobra.io import read_sbml_model, write_sbml_model


def update_cobra_model(cobra_model):
    temp = tempfile.NamedTemporaryFile(prefix='temp_cobra_', suffix='.xml', dir='./', delete=True)
    temp_outfile = temp.name
    temp.close()

    write_sbml_model(cobra_model, temp_outfile, use_fbc_package=False)
    cobra_model = read_sbml_model(temp_outfile)
    os.remove(temp_outfile)
    return cobra_model
