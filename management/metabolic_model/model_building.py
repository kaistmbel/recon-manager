from cobra import Model, Reaction, Metabolite
from management import utils


def model_compartmentalization(cobra_model, compartment_information, default_compartments=['c']):
    compartmentalized_model = Model('Compartmentalized metabolic model')
    for each_reaction in cobra_model.reactions:
        compartments = []
        for each_gene in each_reaction.genes:
            if each_gene.id in compartment_information:
                compartments.append(compartment_information[each_gene.id])

        compartments = list(set(compartments))

        if compartments == []:
            if default_compartments != []:
                compartments = default_compartments

        for each_compartment in compartments:
            new_reaction_metabolite_obj = {}

            for each_metabolite in each_reaction.reactants:
                new_each_metabolite = each_metabolite.id[:-2] + "_" + each_compartment
                new_each_metabolite_name = each_metabolite.name
                new_each_metabolite_formula = each_metabolite.formula
                new_each_metabolite_compartment = each_compartment
                reaction_coff = each_reaction.get_coefficient(each_metabolite)
                new_metabolite_obj = Metabolite(new_each_metabolite, formula=new_each_metabolite_formula,
                                                name=new_each_metabolite_name, compartment=each_compartment)
                new_reaction_metabolite_obj[new_metabolite_obj] = reaction_coff

            for each_metabolite in each_reaction.products:
                new_each_metabolite = each_metabolite.id[:-2] + "_" + each_compartment
                new_each_metabolite_name = each_metabolite.name
                new_each_metabolite_formula = each_metabolite.formula
                new_each_metabolite_compartment = each_compartment
                reaction_coff = each_reaction.get_coefficient(each_metabolite)
                new_metabolite_obj = Metabolite(new_each_metabolite, formula=new_each_metabolite_formula,
                                                name=new_each_metabolite_name, compartment=each_compartment)
                new_reaction_metabolite_obj[new_metabolite_obj] = reaction_coff

            new_reaction_id = '%s_%s' % (each_reaction.id, each_compartment)
            reaction_objective = Reaction(new_reaction_id)
            reaction_objective.name = each_reaction.name
            reaction_objective.subsystem = each_reaction.subsystem
            reaction_objective.lower_bound = each_reaction.lower_bound
            reaction_objective.upper_bound = each_reaction.upper_bound
            reaction_objective.objective_coefficient = each_reaction.objective_coefficient
            reaction_objective.gene_reaction_rule = each_reaction.gene_reaction_rule
            reaction_objective.reversibility = each_reaction.reversibility
            reaction_objective.add_metabolites(new_reaction_metabolite_obj)

            compartmentalized_model.add_reaction(reaction_objective)

    compartmentalized_model = utils.update_cobra_model(compartmentalized_model)
    return compartmentalized_model
