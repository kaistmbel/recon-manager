import copy
import re

from cobra.io import write_sbml_model


class GPR_Converter(object):
    def __init__(self):
        """
        Constructor for GPR converter
        """

    def load_metabolic_model(self, cobra_model):
        self.cobra_model = cobra_model
        return

    def read_mapping_file(self, filename):
        fp = open(filename)
        mapping_identifier_dic = {}
        for line in fp:
            sptlist = line.split('\t')
            entrez_id = sptlist[0].strip()
            ensembl_gene_id = sptlist[1].strip()
            ref_id = sptlist[2].strip()
            ensembl_transcript_id = sptlist[3].strip()
            ucsc_id = sptlist[4].strip()
            ucsc_id = ucsc_id.split('.')[0].strip()

            if entrez_id != '' and ensembl_gene_id != '' and ensembl_transcript_id != '':
                if entrez_id not in mapping_identifier_dic:
                    mapping_identifier_dic[entrez_id] = {}
                    mapping_identifier_dic[entrez_id]['ensembl_gene'] = [ensembl_gene_id]
                    mapping_identifier_dic[entrez_id]['ref_mrna'] = [ref_id]
                    mapping_identifier_dic[entrez_id]['ref_ensembl_transcript'] = [ensembl_transcript_id]
                    mapping_identifier_dic[entrez_id]['ref_ucsc'] = [ucsc_id]
                else:
                    mapping_identifier_dic[entrez_id]['ensembl_gene'].append(ensembl_gene_id)
                    mapping_identifier_dic[entrez_id]['ref_mrna'].append(ref_id)
                    mapping_identifier_dic[entrez_id]['ref_ensembl_transcript'].append(ensembl_transcript_id)
                    mapping_identifier_dic[entrez_id]['ref_ucsc'].append(ucsc_id)
        fp.close()

        for each_key in mapping_identifier_dic:
            mapping_identifier_dic[each_key]['ensembl_gene'] = list(
                set(mapping_identifier_dic[each_key]['ensembl_gene']))
            mapping_identifier_dic[each_key]['ref_mrna'] = list(set(mapping_identifier_dic[each_key]['ref_mrna']))
            mapping_identifier_dic[each_key]['ref_ensembl_transcript'] = list(
                set(mapping_identifier_dic[each_key]['ref_ensembl_transcript']))
            mapping_identifier_dic[each_key]['ref_ucsc'] = list(set(mapping_identifier_dic[each_key]['ref_ucsc']))
        return mapping_identifier_dic

    def load_gene_transcript_mapping_file(self, filename):
        mapping_identifier_dic = self.read_mapping_file(filename)
        self.mapping_identifier_dic = mapping_identifier_dic
        return mapping_identifier_dic

    def convert_GPR_to_TPR(self, output_filename, source):
        cobra_model = copy.deepcopy(self.cobra_model)
        mapping_dic = self.mapping_identifier_dic

        for each_reaction in cobra_model.reactions:
            gpr = each_reaction.gene_reaction_rule
            genes = [gene.id for gene in each_reaction.genes]
            if len(genes) > 0:
                temp_gpr_association_information = {}
                new_mapping_dic = self.update_mapping_dic(mapping_dic, source)

                for each_gene in genes:
                    if each_gene in new_mapping_dic:
                        temp_gpr_association_information[each_gene] = new_mapping_dic[each_gene]
                    else:
                        temp_gpr_association_information[each_gene] = '%s' % (each_gene)

                tpr_string = self.replace_GPR_to_TPR(gpr, temp_gpr_association_information)
                each_reaction.gene_reaction_rule = tpr_string

        write_sbml_model(cobra_model, output_filename, use_fbc_package=False)

    def replace_GPR_to_TPR(self, gpr_string, gpr_information):
        gpr_string = re.sub(r"[(]", r" ( ", gpr_string)
        gpr_string = re.sub(r"[)]", r" ) ", gpr_string)

        for each_gene in gpr_information:
            gene_tpr_list = gpr_information[each_gene]
            gene_tpr_string = ' or '.join(gene_tpr_list)
            gene_tpr_string = ' (%s) ' % (gene_tpr_string)

            gpr_string = re.sub(r"[ ]%s[ ]" % (each_gene), r"%s" % (gene_tpr_string), gpr_string)

        return gpr_string

    def update_mapping_dic(self, mapping_dic, source):
        new_mapping_dic = {}
        if source == 'ensembl_gene':
            source_key = 'ensembl_gene'
        if source == 'ref_mrna':
            source_key = 'ref_mrna'
        if source == 'ensembl_transcript':
            source_key = 'ref_ensembl_transcript'
        if source == 'ucsc':
            source_key = 'ref_ucsc'

        for each_key in mapping_dic:
            if len(mapping_dic[each_key][source_key]) > 0:
                new_mapping_dic[each_key] = mapping_dic[each_key][source_key]
                while True:
                    if '' in new_mapping_dic[each_key]:
                        new_mapping_dic[each_key].remove('')
                    else:
                        break

        return new_mapping_dic
