import json

import pandas as pd
from cobra import Reaction
from management import utils


def make_medium_model(cobra_model, medium_file):
    medium_info = {}
    with open(medium_file, 'r') as fp:
        for each_line in fp:
            sptlist = each_line.strip().split('\t')
            reaction_id = sptlist[0].strip()
            lb = float(sptlist[1].strip())
            ub = float(sptlist[2].strip())
            medium_info[reaction_id] = [lb, ub]
        fp.close()

    for each_reaction in cobra_model.reactions:
        if each_reaction.boundary == 'system_boundary':
            if each_reaction.id in medium_info:
                each_reaction.lower_bound = medium_info[each_reaction.id][0]
                each_reaction.upper_bound = medium_info[each_reaction.id][1]
            else:
                each_reaction.lower_bound = 0.0
                each_reaction.upper_bound = 1000.0
        else:
            if each_reaction.upper_bound > 1000.0:
                each_reaction.upper_bound = 1000.0
            if each_reaction.lower_bound < -1000.0:
                each_reaction.lower_bound = -1000.0

            if each_reaction.reversibility:
                each_reaction.lower_bound = -1000.0
                each_reaction.upper_bound = 1000.0
            else:
                each_reaction.lower_bound = 0.0
                each_reaction.upper_bound = 1000.0

    cobra_model = utils.update_cobra_model(cobra_model)
    return cobra_model


def make_metabolic_task_model(cobra_model, metabolic_task_file):
    for each_reaction in cobra_model.reactions:
        if each_reaction.boundary == 'system_boundary':
            each_reaction.lower_bound = 0.0
            each_reaction.upper_bound = 1000.0

    task_df = pd.read_csv(metabolic_task_file)

    demand_metabolite_info = {}
    medium_information = {}
    for each_row, each_df in task_df.iterrows():
        task_id = each_df.ix['Task ID']
        medium_component_list = each_df.ix['Medium'].split(';')
        if each_df.ix['Type'] == 'Metabolite':
            metabolite = each_df.ix['ID']
            demand_metabolite_info[(metabolite, task_id)] = task_id
        medium_information[task_id] = medium_component_list

    for each_data in demand_metabolite_info:
        each_metabolite = each_data[0]
        task_id = each_data[1]
        cobra_metabolite = cobra_model.metabolites.get_by_id(each_metabolite)
        reaction = Reaction('DMTASK%s_%s' % (task_id, cobra_metabolite.id))
        reaction.name = '%s' % (cobra_metabolite.id)
        reaction.subsystem = 'Demand reaction for %s (TASK ID : %s)' % (each_metabolite, task_id)
        reaction.lower_bound = 0.0
        reaction.upper_bound = 0.0
        reaction.objective_coefficient = 0.0
        reaction.reversibility = False
        reaction.add_metabolites({cobra_metabolite: -1.0})
        cobra_model.add_reactions([reaction])

    for task_id in medium_information:
        medium_component_list = medium_information[task_id]
        reaction = Reaction('TASK_MEDIUM_%s' % (task_id))
        medium_metabolites = {}
        for each_metabolite_information in medium_component_list:
            sptlist = each_metabolite_information[:-1].split('(')
            metabolite = sptlist[0].strip()
            uptake_rate = float(sptlist[1].strip())
            cobra_metabolite = cobra_model.metabolites.get_by_id(metabolite)
            medium_metabolites[cobra_metabolite] = uptake_rate

        reaction.name = 'TASK_MEDIUM_%s' % (task_id)
        reaction.subsystem = 'Medium reaction for TASK %s' % (task_id)
        reaction.lower_bound = 0.0
        reaction.upper_bound = 0.0
        reaction.objective_coefficient = 0.0
        reaction.reversibility = True

        reaction.add_metabolites(medium_metabolites)
        cobra_model.add_reactions([reaction])

    cobra_model = utils.update_cobra_model(cobra_model)
    return cobra_model


def parse_MNXref_prop(MNXref_prop):
    MNXref_prop_info = {}
    with open(MNXref_prop, 'r') as fp:
        for line in fp:
            if line[0] != '#':
                sptlist = line.strip().split('\t')
                mnx_id = sptlist[0].strip()
                description = sptlist[1].strip()
                formula = sptlist[2].strip()
                charge = sptlist[3].strip()
                inchi = sptlist[5].strip()
                smiles = sptlist[6].strip()
                MNXref_prop_info[mnx_id] = {}
                MNXref_prop_info[mnx_id]['description'] = description
                MNXref_prop_info[mnx_id]['formula'] = formula
                MNXref_prop_info[mnx_id]['charge'] = charge
                MNXref_prop_info[mnx_id]['inchi'] = inchi
                MNXref_prop_info[mnx_id]['smiles'] = smiles
    return MNXref_prop_info


def parse_MNXref_xref(MNXref_xref):
    target_databases = ['bigg', 'kegg', 'hmdb', 'metacyc']  # 'chebi'
    MNXref_xref_info = {}
    with open(MNXref_xref, 'r') as fp:
        for line in fp:
            if line[0] != '#':
                sptlist = line.strip().split('\t')
                xref_info = sptlist[0].strip()
                xref_db = xref_info.split(':')[0].strip()
                xref_id = xref_info.split(':')[1].strip()
                mnx_id = sptlist[1].strip()

                if xref_db == 'chebi':
                    xref_id = 'CHEBI:%s' % (xref_id)

                if xref_db in target_databases:
                    if mnx_id not in MNXref_xref_info:
                        MNXref_xref_info[mnx_id] = ['%s(%s)' % (xref_id, xref_db.upper())]
                    else:
                        MNXref_xref_info[mnx_id].append('%s(%s)' % (xref_id, xref_db.upper()))

    return MNXref_xref_info


def parse_BIGG_metabolite(BIGG_data):
    db_name_info = {}
    db_name_info['Human Metabolome Database'] = 'hmdb'
    db_name_info['KEGG Compound'] = 'kegg'
    db_name_info['MetaNetX (MNX) Chemical'] = 'MNXref'
    # db_name_info['CHEBI'] = 'chebi'
    db_name_info['BioCyc'] = 'metacyc'

    BIGG_ref_info = {}
    with open(BIGG_data, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            bigg_id = sptlist[1].strip()
            db_links = sptlist[4].strip()

            jsonString = db_links
            db_link_info = json.loads(jsonString)
            if 'MetaNetX (MNX) Chemical' in db_link_info:
                mnx_id = db_link_info['MetaNetX (MNX) Chemical'][0]['id']
                BIGG_ref_info[mnx_id] = {}
                xref_list = []
                for each_db_name in db_link_info:
                    if each_db_name in db_name_info:
                        new_db_id = db_name_info[each_db_name]
                        for each_data in db_link_info[each_db_name]:
                            xref_list.append('%s(%s)' % (each_data['id'], new_db_id.upper()))
                BIGG_ref_info[mnx_id] = xref_list
    return BIGG_ref_info


def parse_CHEBI_metabolite(CHEBI_inchi_data):
    inchi_chebi_information = {}
    with open(CHEBI_inchi_data, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            chebi_id = sptlist[0].strip()
            inchi_string = sptlist[1].strip()
            if inchi_string not in inchi_chebi_information:
                inchi_chebi_information[inchi_string] = [chebi_id]
            else:
                inchi_chebi_information[inchi_string].append(chebi_id)
    return inchi_chebi_information


def update_metabolite_information(cobra_model, BIGG_data, CHEBI_inchi_data, MNXref_prop, MNXref_xref):
    MNXref_prop_info = parse_MNXref_prop(MNXref_prop)
    MNXref_xref_info = parse_MNXref_xref(MNXref_xref)
    BIGG_ref_info = parse_BIGG_metabolite(BIGG_data)
    CHEBI_inchi_info = parse_CHEBI_metabolite(CHEBI_inchi_data)
    metabolite_information = {}
    for each_metabolite in cobra_model.metabolites:
        metabolite_id = each_metabolite.id[:-2]
        metabolite_information[each_metabolite.id] = {}
        metabolite_information[each_metabolite.id]['name'] = each_metabolite.name
        metabolite_information[each_metabolite.id]['formula'] = 'N/A'
        metabolite_information[each_metabolite.id]['charge'] = 'N/A'
        metabolite_information[each_metabolite.id]['inchi'] = 'N/A'
        metabolite_information[each_metabolite.id]['smiles'] = 'N/A'
        metabolite_information[each_metabolite.id]['Reference'] = 'N/A'

        each_metabolite.notes = {}
        if metabolite_id in MNXref_xref_info:
            MNXref_db_links = MNXref_xref_info[metabolite_id]

            if each_metabolite.id in BIGG_ref_info:
                bigg_db_links = BIGG_ref_info[metabolite_id]
                MNXref_db_links = list(set(MNXref_db_links) & set(bigg_db_links))

            chebi_list = []
            if metabolite_id in MNXref_prop_info:
                inchi = MNXref_prop_info[metabolite_id]['inchi']
                if inchi in CHEBI_inchi_info:
                    for each_chebi in CHEBI_inchi_info[inchi]:
                        chebi_list.append('CHEBI:%s(CHEBI)' % (each_chebi))

            MNXref_db_links = MNXref_db_links + chebi_list
            each_metabolite.notes['REFERENCE'] = ';'.join(MNXref_db_links)
            metabolite_information[each_metabolite.id]['REFERENCE'] = ';'.join(MNXref_db_links)

        if metabolite_id in MNXref_prop_info:
            description = MNXref_prop_info[metabolite_id]['description']
            formula = MNXref_prop_info[metabolite_id]['formula']
            charge = MNXref_prop_info[metabolite_id]['charge']
            inchi = MNXref_prop_info[metabolite_id]['inchi']
            smiles = MNXref_prop_info[metabolite_id]['smiles']

            each_metabolite.charge = 'N/A'
            each_metabolite.formula = 'N/A'
            each_metabolite.name = 'N/A'

            if charge.strip() != '':
                each_metabolite.charge = int(charge)
            else:
                each_metabolite.charge = 0

            if formula.strip() != '':
                each_metabolite.formula = formula
            if description.strip() != '':
                each_metabolite.name = description

            if charge.strip() == '':
                charge = 'N/A'
            if formula.strip() == '':
                formula = 'N/A'

            metabolite_information[each_metabolite.id]['name'] = each_metabolite.name
            metabolite_information[each_metabolite.id]['formula'] = formula
            metabolite_information[each_metabolite.id]['charge'] = charge
            metabolite_information[each_metabolite.id]['inchi'] = inchi
            metabolite_information[each_metabolite.id]['smiles'] = smiles

            each_metabolite.notes['INCHI'] = inchi
            each_metabolite.notes['SMILES'] = smiles

    return cobra_model, metabolite_information
