import copy
import time
import logging

from cobra import Reaction
from cobra.flux_analysis.variability import find_blocked_reactions
from management.metabolic_simulation import Simulator


def calculate_model_statistics(cobra_model):
    model_statistics_info = {}
    balanced_reaction_cnt = 0
    gene_mediated_reactions_cnt = 0
    for each_reaction in cobra_model.reactions:
        try:
            result = each_reaction.check_mass_balance()
            if result == {}:
                balanced_reaction_cnt += 1
        except:
            pass
        if len(each_reaction.genes) > 0:
            gene_mediated_reactions_cnt += 1

    unique_metabolite_list = []
    reference_cnt = 0
    formula_cnt = 0
    structural_information_cnt = 0
    
    logging.info("Calculating statistics of model contents")
    start = time.time()
    for each_metabolite in cobra_model.metabolites:
        unique_metabolite_list.append(each_metabolite.id[:-2])
        if 'REFERENCE' in each_metabolite.notes:
            ref = each_metabolite.notes['REFERENCE'][0].strip()
            if ref != '':
                reference_num = ref.count('(')
                if 'BIGG' in ref:
                    if reference_num >= 2:
                        reference_cnt += 1
                else:
                    reference_cnt += 1

        if each_metabolite.formula != '' and each_metabolite.formula.strip() != 'N/A':
            formula_cnt += 1

        structural_information = False
        if 'INCHI' in each_metabolite.notes:
            inchi = each_metabolite.notes['INCHI'][0].strip()
            if inchi.strip() != '' and inchi.strip() != 'N/A':
                structural_information = True
        if 'SMILES' in each_metabolite.notes:
            smiles = each_metabolite.notes['SMILES'][0].strip()
            if smiles.strip() != '' and smiles.strip() != 'N/A':
                structural_information = True
        if structural_information:
            structural_information_cnt += 1
            
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
    unique_metabolite_list = list(set(unique_metabolite_list))
    
    model_statistics_info['Total number of reactions'] = len(cobra_model.reactions)
    model_statistics_info['Total number of metabolites'] = len(cobra_model.metabolites)
    model_statistics_info['Number of unique metabolites'] = len(unique_metabolite_list)

    model_statistics_info['Number of unique genes'] = len(cobra_model.genes)
    model_statistics_info['Number of balanced reactions'] = '%s (%s)' % (
        balanced_reaction_cnt, balanced_reaction_cnt / float(len(cobra_model.reactions)) * 100)
    model_statistics_info['Number of gene-mediated reactions'] = '%s (%s)' % (
        gene_mediated_reactions_cnt, gene_mediated_reactions_cnt / float(len(cobra_model.reactions)) * 100)

    model_statistics_info['Number of metabolites with formula'] = '%s (%s)' % (
        formula_cnt, formula_cnt / float(len(cobra_model.metabolites)) * 100)
    model_statistics_info['Number of metabolites with structural information'] = '%s (%s)' % (
        structural_information_cnt, structural_information_cnt / float(len(cobra_model.metabolites)) * 100)
    model_statistics_info['Number of metabolites with reference information'] = '%s (%s)' % (
        reference_cnt, reference_cnt / float(len(cobra_model.metabolites)) * 100)
    
    blocked_reactions = []
    dead_end_metabolites = []
    
    for each_reaction in cobra_model.reactions:
        if each_reaction.boundary == 'system_boundary':
            each_reaction.lower_bound = -1000.0
            each_reaction.upper_bound = 1000.0
            
    logging.info("Finding blocked reactions")
    start = time.time()
    blocked_reactions = find_blocked_reactions(cobra_model)        
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
    dead_end_metabolites = calculate_dead_end_metabolites(cobra_model)
    
    model_statistics_info['Number of blocked reactions'] = '%s (%s)' % (
        len(blocked_reactions), len(blocked_reactions) / float(len(cobra_model.reactions)) * 100)
    model_statistics_info['Number of dead-end metabolites'] = '%s (%s)' % (
        len(dead_end_metabolites), len(dead_end_metabolites) / float(len(cobra_model.metabolites)) * 100)
    
    return model_statistics_info


def calculate_blocked_reactions(cobra_model):
    logging.info("Finding blocked reactions")
    start = time.time()
    constraints = {}
    for each_reaction in cobra_model.reactions:
        if each_reaction.boundary == 'system_boundary':
            constraints[each_reaction.id] = [each_reaction.lower_bound, each_reaction.upper_bound]
            each_reaction.lower_bound = -1000.0
            each_reaction.upper_bound = 1000.0
    blocked_reactions = []
    simulation_obj = Simulator.Simulator()
    simulation_obj.load_cobra_model(cobra_model)
    for each_reaction in cobra_model.reactions:
        solution_status1, obj_val1, max_flux_distribution = simulation_obj.run_FBA(new_objective=each_reaction.id,
                                                                                   mode='max')
        solution_status2, obj_val2, min_flux_distribution = simulation_obj.run_FBA(new_objective=each_reaction.id,
                                                                                   mode='min')
        if abs(max_flux_distribution[each_reaction.id]) == 0.0 and abs(min_flux_distribution[each_reaction.id]) == 0.0:
            blocked_reactions.append(each_reaction.id)

    for each_reaction in cobra_model.reactions:
        if each_reaction.id in constraints:
            each_reaction.lower_bound = constraints[each_reaction.id][0]
            each_reaction.upper_bound = constraints[each_reaction.id][1]
    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return blocked_reactions


def calculate_dead_end_metabolites(cobra_model):
    logging.info("Finding dead-end metabolites")
    start = time.time()
    constraints = {}
    for each_reaction in cobra_model.reactions:
        if each_reaction.boundary == 'system_boundary':
            constraints[each_reaction.id] = [each_reaction.lower_bound, each_reaction.upper_bound]
            each_reaction.lower_bound = -1000.0
            each_reaction.upper_bound = 1000.0
    dead_end_metabolites = []
    simulation_obj = Simulator.Simulator()
    simulation_obj.load_cobra_model(cobra_model)

    dead_end_test_cobra_model = copy.deepcopy(cobra_model)
    for each_metabolite in dead_end_test_cobra_model.metabolites:
        if is_dead_end_metabolite(dead_end_test_cobra_model, each_metabolite):
            dead_end_metabolites.append(each_metabolite.id)

    for each_reaction in cobra_model.reactions:
        if each_reaction.id in constraints:
            each_reaction.lower_bound = constraints[each_reaction.id][0]
            each_reaction.upper_bound = constraints[each_reaction.id][1]
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    return dead_end_metabolites


def is_dead_end_metabolite(cobra_model, cobra_metabolite):
    each_metabolite = cobra_metabolite
    reaction = Reaction('DeadEndMet_%s' % (cobra_metabolite.id))
    reaction.name = '%s' % (cobra_metabolite.id)
    reaction.subsystem = 'Test'
    reaction.lower_bound = -1000.  # This is the default
    reaction.upper_bound = 1000.  # This is the default
    reaction.objective_coefficient = 0.  # this is the default
    reaction.add_metabolites({cobra_metabolite: -1.0})
    cobra_model.add_reactions([reaction])

    simulation_obj = Simulator.Simulator()
    simulation_obj.load_cobra_model(cobra_model)

    solution_status1, obj_val1, max_flux_distribution = simulation_obj.run_FBA(new_objective=reaction.id, mode='max')
    solution_status2, obj_val2, min_flux_distribution = simulation_obj.run_FBA(new_objective=reaction.id, mode='min')

    cobra_model.remove_reactions([reaction])
    if abs(max_flux_distribution[reaction.id]) == 0.0 and abs(min_flux_distribution[reaction.id]) == 0.0:
        return True
    else:
        return False
