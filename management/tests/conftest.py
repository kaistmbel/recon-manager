from os.path import join, abspath, dirname

import pytest

data_model_dir = join(dirname(abspath(__file__)), 'data')


@pytest.fixture(scope="function")
def model_file():
    model_file = join(data_model_dir, 'Recon2M.2_Entrez_Gene.xml')
    return model_file


@pytest.fixture(scope="function")
def expression_file():
    expression_file = join(data_model_dir, 'FPKM(adrenal_4a.V119).csv')
    return expression_file


@pytest.fixture(scope="function")
def expression_file_rank():
    expression_file_rank = join(data_model_dir, 'FPKM(adrenal_4a.V119)_rank_precalculated.csv')
    return expression_file_rank


@pytest.fixture(scope="function")
def expression_file_original():
    expression_file_original = join(data_model_dir, 'FPKM(adrenal_4a.V119)_original_precalculated.csv')
    return expression_file_original


@pytest.fixture(scope="function")
def reaction_weight_file():
    reaction_weight_file = join(data_model_dir, 'FPKM(adrenal_4a.V119)_rank_reaction_weight_precalculated.csv')
    return reaction_weight_file


@pytest.fixture(scope="function")
def medium_file():
    medium_file = join(data_model_dir, 'RPMI1640_medium.txt')
    return medium_file


@pytest.fixture(scope="function")
def metabolic_task_file():
    metabolic_task_file = join(data_model_dir, 'MetabolicTasks.csv')
    return metabolic_task_file


@pytest.fixture(scope="function")
def present_metabolite_file():
    present_metabolite_file = join(data_model_dir, 'essential_metabolites.txt')
    return present_metabolite_file


@pytest.fixture(scope="function")
def essential_reaction_file():
    essential_reaction_file = join(data_model_dir, 'essential_reactions.txt')
    return essential_reaction_file


@pytest.fixture(scope="function")
def recon2M2_task_result_file():
    recon2M2_task_result_file = join(data_model_dir, 'Recon2M.2_metabolic_task.csv')
    return recon2M2_task_result_file


@pytest.fixture(scope="function")
def gap_model_file():
    gap_model_file = join(data_model_dir, 'Gap_model.xml')
    return gap_model_file


@pytest.fixture(scope="function")
def gap_filled_model_file():
    gap_filled_model_file = join(data_model_dir, 'Gapfilling_functional_model.xml')
    return gap_filled_model_file


@pytest.fixture(scope="function")
def gap_filling_result_file():
    gap_filling_result_file = join(data_model_dir, 'gap_filling_result.csv')
    return gap_filling_result_file
