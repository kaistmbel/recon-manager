import pandas as pd
from cobra.io import read_sbml_model
from management.metabolic_simulation import metabolic_task


class TestMetabolicTask:
    """Test functions in management.metabolic_simulation.metabolic_task"""

    def test_evaluate_metabolic_task(self, model_file, metabolic_task_file, recon2M2_task_result_file):
        precalculated_task_result = pd.read_csv(recon2M2_task_result_file)

        cobra_model = read_sbml_model(model_file)

        task_result = metabolic_task.evaluate_metabolic_task(cobra_model, metabolic_task_file)

        for each_column in ['Task ID', 'Type', 'ID', 'Medium', 'Constraints', 'Expected value', 'Description',
                            'Task result',
                            'Result equation', 'Solution status']:
            assert each_column in task_result.columns

        for each_index in task_result.index:
            for each_column in task_result.columns:
                assert task_result.ix[each_index][each_column] == precalculated_task_result.ix[each_index][each_column]
