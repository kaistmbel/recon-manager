from cobra.io import read_sbml_model
from management.metabolic_simulation import Simulator


class TestSimulator:
    """Test functions in management.metabolic_simulation.Simulator"""

    def test_read_model(self, model_file):
        model_obj = Simulator.Simulator()
        model_obj.read_model(model_file)
        assert len(model_obj.model_reactions) == 5837
        assert len(model_obj.model_metabolites) == 3368
        assert len(model_obj.model_genes) == 1656

    def test_load_model(self, model_file):
        biomass_reaction = 'biomass_reaction'
        cobra_model = read_sbml_model(model_file)
        model_obj = Simulator.Simulator()
        model_obj.load_cobra_model(cobra_model)
        assert len(model_obj.model_reactions) == 5837
        assert len(model_obj.model_metabolites) == 3368
        assert len(model_obj.model_genes) == 1656

    def test_FBA(self, model_file):
        biomass_reaction = 'biomass_reaction'
        model_obj = Simulator.Simulator()
        model_obj.read_model(model_file)
        solution_status, objective_value, flux_distribution = model_obj.run_FBA(new_objective=biomass_reaction,
                                                                                internal_flux_minimization=False)  # FBA
        biomass_flux = flux_distribution[biomass_reaction]
        assert str(biomass_flux)[0:5] == '0.118'
        assert solution_status == 2, '2 is optimal solution status'

        solution_status, objective_value, flux_distribution = model_obj.run_FBA(new_objective=biomass_reaction,
                                                                                internal_flux_minimization=True)  # pFBA
        biomass_flux = flux_distribution[biomass_reaction]
        assert str(biomass_flux)[0:5] == '0.118'
        assert solution_status == 2, '2 is optimal solution status'

    def test_MOMA(self, model_file):
        biomass_reaction = 'biomass_reaction'
        target_reaction = 'PGI'
        model_obj = Simulator.Simulator()
        model_obj.read_model(model_file)
        wild_solution_status, wild_objective_value, wild_type_flux = model_obj.run_FBA(new_objective=biomass_reaction,
                                                                                       internal_flux_minimization=True)  # pFBA
        assert wild_solution_status == 2, '2 is optimal solution status'
        flux_constraints = {}
        flux_constraints[target_reaction] = [0.0, 0.0]
        perturbed_solution_status, objective_value, perturbed_flux_distribution = model_obj.run_MOMA(
            wild_flux=wild_type_flux, flux_constraints=flux_constraints)  # MOMA
        assert perturbed_solution_status == 2, '2 is optimal solution status'
        assert perturbed_flux_distribution[target_reaction] == 0.0
