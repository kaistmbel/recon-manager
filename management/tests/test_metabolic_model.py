from cobra.io import read_sbml_model
from management.metabolic_model import GPR_manipulation
from management.metabolic_model import model_statistics


class TestMetabolicModel:
    """Test functions in management.metabolic_simulation.Simulator"""

    def test_model_statistics(self, model_file):
        cobra_model = read_sbml_model(model_file)
        model_statistics_info = model_statistics.calculate_model_statistics(cobra_model)

        assert 'num_of_reactions' in model_statistics_info
        assert 'num_of_metabolites' in model_statistics_info
        assert 'num_of_genes' in model_statistics_info
        assert 'num_of_balanced_reactions' in model_statistics_info
        assert 'num_of_gene_mediated_reactions' in model_statistics_info

        assert model_statistics_info['num_of_reactions'] == 5837
        assert model_statistics_info['num_of_metabolites'] == 3368
        assert model_statistics_info['num_of_genes'] == 1656
        assert model_statistics_info['num_of_balanced_reactions'] == 4997
        assert model_statistics_info['num_of_gene_mediated_reactions'] == 3937

    def test_GPR_manipuation(self):
        case1 = GPR_manipulation.convert_string_GPR_to_list_GPR("(a AND b) OR (c OR d OR e)")
        case2 = GPR_manipulation.convert_string_GPR_to_list_GPR("(a AND b) OR (c OR d AND e)")
        case3 = GPR_manipulation.convert_string_GPR_to_list_GPR("(a AND b) OR c OR d AND e")

        assert case1 == [['a', 'AND', 'b'], 'OR', ['c', 'OR', 'd', 'OR', 'e']]
        assert case2 == [['a', 'AND', 'b'], 'OR', ['c', 'OR', 'd', 'AND', 'e']]
        assert case3 == [['a', 'AND', 'b'], 'OR', 'c', 'OR', 'd', 'AND', 'e']
