import pandas as pd
from cobra.io import read_sbml_model
from management.metabolic_model import model_editing
from management.metabolic_simulation import Gap
from management.metabolic_simulation import gap_filling


class TestGapFilling:
    """Test functions in management.metabolic_simulation.Gap"""

    def test_Gap(self, model_file, gap_model_file, medium_file):

        generic_cobra_model = read_sbml_model(model_file)
        generic_cobra_model = model_editing.make_medium_model(generic_cobra_model, medium_file)

        gap_cobra_model = read_sbml_model(gap_model_file)
        gap_cobra_model = model_editing.make_medium_model(gap_cobra_model, medium_file)

        target_reaction = 'PGI'
        obj = Gap.GapFilling()
        model_status, objective_value, added_reactions, reaction_object_list = obj.run_gap_filling(generic_cobra_model,
                                                                                                   gap_cobra_model,
                                                                                                   target_reaction)
        assert model_status == 2
        assert objective_value == 1
        assert added_reactions[0] == target_reaction

        target_reaction = 'PGM'
        obj = Gap.GapFilling()
        model_status, objective_value, added_reactions, reaction_object_list = obj.run_gap_filling(generic_cobra_model,
                                                                                                   gap_cobra_model,
                                                                                                   target_reaction)
        assert model_status == 2
        assert objective_value == 1
        assert added_reactions[0] == target_reaction

    def test_fill_functional_metabolic_gaps(self, model_file, gap_model_file, metabolic_task_file,
                                            gap_filled_model_file, gap_filling_result_file):
        generic_cobra_model = read_sbml_model(model_file)
        gap_cobra_model = read_sbml_model(gap_model_file)

        pre_gap_filled_model = read_sbml_model(gap_filled_model_file)
        precalculated_gap_filling_result = pd.read_csv(gap_filling_result_file)

        functional_model, gap_filling_result_task_df, draft_task_df, failed_task_df = gap_filling.fill_functional_metabolic_gaps(
            generic_cobra_model, gap_cobra_model, metabolic_task_file)

        pre_defined_model_reactions = [reaction.id for reaction in pre_gap_filled_model.reactions]
        gap_filled_model_reactions = [reaction.id for reaction in functional_model.reactions]

        for each_reaction in pre_defined_model_reactions:
            assert each_reaction in gap_filled_model_reactions

        for each_column in ['Task ID', 'Type', 'ID', 'Medium', 'Constraints', 'Expected value', 'Description',
                            'Gap filling result', 'Added reaction',
                            'Solution status', 'Solution value']:
            assert each_column in gap_filling_result_task_df.columns

        for each_task_id in list(gap_filling_result_task_df['Task ID']):
            for each_column in ['Task ID', 'Type', 'ID', 'Medium', 'Constraints', 'Expected value', 'Description',
                                'Gap filling result', 'Added reaction',
                                'Solution status']:
                assert \
                    gap_filling_result_task_df[gap_filling_result_task_df['Task ID'] == each_task_id][
                        each_column].values[
                        0] == \
                    precalculated_gap_filling_result[precalculated_gap_filling_result['Task ID'] == each_task_id][
                        each_column].values[0]
