import pandas as pd
from cobra.io import read_sbml_model
from management.metabolic_model import model_editing


class TestModelEditing:
    """Test functions in management.metabolic_model.model_editing"""

    def test_make_metabolic_task_model(self, model_file, metabolic_task_file):
        cobra_model = read_sbml_model(model_file)
        task_df = pd.read_csv(metabolic_task_file)
        metabolite_task_df = task_df[task_df['Type'] == 'Metabolite']

        metabolic_task_cobra_model = model_editing.make_metabolic_task_model(cobra_model, metabolic_task_file)
        model_reactions = [each_reaction.id for each_reaction in metabolic_task_cobra_model.reactions]

        task_reaction_count = 0
        for each_reaction in metabolic_task_cobra_model.reactions:
            if each_reaction.boundary == 'system_boundary':
                assert each_reaction.lower_bound >= 0
            if 'DMTASK' in each_reaction.id:
                task_reaction_count += 1

        assert task_reaction_count == len(list(metabolite_task_df['Task ID']))

        for each_task_id in list(task_df['Task ID']):
            assert 'TASK_MEDIUM_%s' % (each_task_id) in model_reactions

    def test_make_medium_model(self, model_file, medium_file):
        cobra_model = read_sbml_model(model_file)
        medium_cobra_model = model_editing.make_medium_model(cobra_model, medium_file)
        medium_source_cnt = 0
        for each_reaction in medium_cobra_model.reactions:
            if each_reaction.boundary == 'system_boundary':
                if each_reaction.lower_bound < 0.0:
                    medium_source_cnt += 1
        assert medium_source_cnt == 43

        medium_information = {}
        with open(medium_file, 'r') as fp:
            for line in fp:
                sptlist = line.strip().split('\t')
                medium_information[sptlist[0]] = [float(sptlist[1].strip()), float(sptlist[2].strip())]

        for each_reaction in medium_information:
            assert medium_cobra_model.reactions.get_by_id(each_reaction).lower_bound == \
                   medium_information[each_reaction][0]
            assert medium_cobra_model.reactions.get_by_id(each_reaction).upper_bound == \
                   medium_information[each_reaction][1]
