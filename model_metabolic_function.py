import argparse
import os
import time
import warnings
import logging
import pandas as pd
from cobra.flux_analysis.variability import find_blocked_reactions
from cobra.io import read_sbml_model, write_sbml_model
from management.metabolic_model import model_editing
from management.metabolic_simulation import metabolic_task


def main():
    start = time.time()
    warnings.filterwarnings("ignore")

    parser = argparse.ArgumentParser()

    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-model', '--cobra_model_file', required=True, help="Cobra model file")
    parser.add_argument('-medium', '--medium_file', required=True, help="Medium file")
    parser.add_argument('-defined_medium', '--defined_medium_file', required=True, help="Defined medium file")
    parser.add_argument('-es_genes', '--essential_gene_file', required=True, help="Essential gene file")
    parser.add_argument('-ne_genes', '--nonessential_gene_file', required=True, help="Nonessential gene file")
    parser.add_argument('-c_source', '--carbon_source_list_file', required=True, help="Carbon source list file")
    parser.add_argument('-biomass', '--biomass_reaction', required=True, help="Biomass reaction")
    parser.add_argument('-oxygen', '--oxygen_uptake_reaction', required=True, help="Oxygen uptake reaction")
    parser.add_argument('-atp', '--atp_demand_reaction', required=True, help="ATP demand reaction")

    
    use_essential_gene_sampling = False

    options = parser.parse_args()

    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
        
    output_dir = options.output_dir
    model_file = options.cobra_model_file
    medium_file = options.medium_file
    defined_medium_file = options.defined_medium_file
    essential_gene_file = options.essential_gene_file
    non_essential_gene_file = options.nonessential_gene_file
    biomass_reaction = options.biomass_reaction
    oxygen_uptake_reaction = options.oxygen_uptake_reaction
    atp_demand_reaction = options.atp_demand_reaction
    atp_carbon_source_list_file = options.carbon_source_list_file

    try:
        os.mkdir(output_dir)
    except:
        pass

    ## read cobra model
    basename = os.path.basename(model_file)
    cobra_model = read_sbml_model(model_file)
    
    logging.info("Removing blocked reactions")
    start = time.time()
    blocked_reactions = find_blocked_reactions(cobra_model)    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
    cobra_model.remove_reactions(blocked_reactions)
    write_sbml_model(cobra_model, output_dir + 'Blocked_reaction_removed_%s' % (basename), use_fbc_package=False)

    blocked_cobra_model = read_sbml_model(output_dir + 'Blocked_reaction_removed_%s' % (basename))
    simulation_cobra_model = model_editing.make_medium_model(blocked_cobra_model, medium_file)

    cobra_model = read_sbml_model(model_file)
    defined_medium_model = model_editing.make_medium_model(cobra_model, defined_medium_file)

    ## gene essentiality
    essentiality_result_information, accuracy, specificity, sensitivity = metabolic_task.essentiality_simulation(
        simulation_cobra_model, biomass_reaction, essential_gene_file, non_essential_gene_file,
        use_essential_gene_sampling)

    with open(output_dir + 'gene_essentiality.txt', 'w') as fp:
        print >> fp, 'Accuracy\t%s' % (accuracy)
        print >> fp, 'Specificity\t%s' % (specificity)
        print >> fp, 'Sensitivity\t%s' % (sensitivity)
        print >> fp, '####################################'
        print >> fp, 'Detailed information'
        print >> fp, '####################################'
        for each_gene in essentiality_result_information:
            target_reactions = essentiality_result_information[each_gene]['reactions']
            in_silico_result = essentiality_result_information[each_gene]['insilico']
            experimental_result = essentiality_result_information[each_gene]['experimental']
            print >> fp, '%s\t%s\t%s\t%s\t' % (
                each_gene, experimental_result, in_silico_result, ';'.join(target_reactions))

    ## oxygen simulation
    results = metabolic_task.oxygen_response_simulation(simulation_cobra_model, biomass_reaction,
                                                        oxygen_uptake_reaction)
    df = pd.DataFrame.from_dict(results)
    df.to_csv(output_dir + 'oygen_level_simulation_result.csv')

    ## ATP production rate calculation
    results = metabolic_task.calculate_atp_production_rate(defined_medium_model, atp_demand_reaction,
                                                           oxygen_uptake_reaction, atp_carbon_source_list_file)
    df = pd.DataFrame.from_dict(results)
    df.to_csv(output_dir + 'ATP_production_rate.csv')
    
    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))

if __name__ == '__main__':
    main()
